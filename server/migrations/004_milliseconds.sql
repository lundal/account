update apps set created = created * 1000;
update app_sessions set created = created * 1000, expires = expires * 1000;
update users set created = created * 1000;
update user_sessions set created = created * 1000, expires = expires * 1000;
update user_invites set created = created * 1000, expires = expires * 1000;
