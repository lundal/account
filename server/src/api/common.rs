use derive_more::{AsRef, Into};
use regex::Regex;
use serde::{Deserialize, Serialize};

#[derive(Clone, Serialize, Deserialize, Into, AsRef)]
#[serde(try_from = "String", into = "String")]
pub struct Username(String);

impl TryFrom<String> for Username {
    type Error = String;

    fn try_from(value: String) -> Result<Self, Self::Error> {
        if !Regex::new(r"^[A-Za-z0-9]{3,60}$").unwrap().is_match(&value) {
            Err("username must be 3-60 alphanumeric characters".into())
        } else {
            Ok(Self(value))
        }
    }
}

#[derive(Clone, Serialize, Deserialize, Into, AsRef)]
#[serde(try_from = "String", into = "String")]
pub struct Password(String);

impl TryFrom<String> for Password {
    type Error = String;

    fn try_from(value: String) -> Result<Self, Self::Error> {
        if !Regex::new(r"^.{12,60}$").unwrap().is_match(&value) {
            Err("password must be 12-60 characters".into())
        } else {
            Ok(Self(value))
        }
    }
}

#[derive(Clone, Serialize, Deserialize, Into, AsRef)]
#[serde(try_from = "String", into = "String")]
pub struct Invite(String);

impl TryFrom<String> for Invite {
    type Error = String;

    fn try_from(value: String) -> Result<Self, Self::Error> {
        if !Regex::new(r"^[A-Za-z0-9]{24}$").unwrap().is_match(&value) {
            Err("invite must be 24 alphanumeric characters".into())
        } else {
            Ok(Self(value))
        }
    }
}
