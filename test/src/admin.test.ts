import { expect, test } from "@playwright/test";
import { goto, signIn } from "./utils";

test.beforeEach(async ({ page }) => {
  await signIn(page, "admin");
});

test.describe("Account", () => {
  test.beforeEach(async ({ page }) => {
    await goto(page, "/account");
  });

  test("should display username", async ({ page }) => {
    await expect(page.locator("main")).toContainText("admin");
  });

  test("should display nav links", async ({ page }) => {
    await expect(page.locator("nav a")).toHaveText([
      "Account",
      "Security",
      "Accounts",
      "Invites",
    ]);
  });
});

test.describe("Accounts", () => {
  test.beforeEach(async ({ page }) => {
    await goto(page, "/accounts");
  });

  test("should list self", async ({ page }) => {
    await expect(page.locator("table")).toContainText("admin");
  });
});

test.describe("Invites", () => {
  test.beforeEach(async ({ page }) => {
    await goto(page, "/invites");
  });

  test("should allow create invite", async ({ page }) => {
    await page.waitForLoadState("networkidle");
    const invites = await page.locator("tbody tr").count();
    await page.locator("text=Create invite").click();
    await page.waitForLoadState("networkidle");
    await expect(page.locator("tbody tr")).toHaveCount(invites + 1);
  });
});
