import {
  equals,
  length,
  maxLength,
  minLength,
  regex,
  required,
  validator,
} from "../utils/ValidationUtils";

export const validateName = validator([
  [required(), "Name is required"],
  [regex(/^[A-Za-z0-9]+$/), "Name must contain only letters and numbers"],
  [minLength(3), "Name must be at least 3 characters"],
  [maxLength(64), "Name must be at most 64 characters"],
]);

export const validatePassword = validator([
  [required(), "Password is required"],
  [minLength(12), "Password must be at least 12 characters"],
  [maxLength(64), "Password must be at most 64 characters"],
]);

export const validatePasswordAgain = (password: string) =>
  validator([[equals(password), "Passwords do not match"]]);

export const validateInvite = validator([
  [required(), "Invite is required"],
  [length(24), "Invite must be exactly 24 characters"],
]);
