use sqlx::migrate::Migrator;
use sqlx::postgres::PgPoolOptions;
use sqlx::{migrate, Error, Pool, Postgres};

pub use accounts::*;
pub use invites::*;
pub use sessions::*;

mod accounts;
mod invites;
mod sessions;

static MIGRATOR: Migrator = migrate!("./migrations");

pub async fn connect(url: &str) -> Result<Pool<Postgres>, Error> {
    let pool = PgPoolOptions::new().connect(&url).await?;
    MIGRATOR.run(&pool).await?;
    Ok(pool)
}
