# Account

Single sign on and user account management for lundal apps.

### Development

1. Start dependencies
   ```bash
   docker-compose up --detached
   ```
2. Start backend
   ```bash
   cd server
   cargo run --release
   ```
3. Start frontend with hot module reloading
   ```bash
   cd web
   npm install
   npm run dev
   ```
4. Run e2e tests
   ```bash
   cd test
   yarn install
   npx playwright install
   yarn test
   ```
