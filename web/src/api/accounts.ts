import { request } from "@lundal/request";
import {
  CreateAccount,
  DeleteAccount,
  SignIn,
  ChangePassword,
  Account,
} from "./models";

export function createAccount(account: CreateAccount): Promise<Account> {
  return request({
    method: "POST",
    url: "/api/v1/accounts",
    body: account,
  });
}

export function listAccounts(): Promise<Account[]> {
  return request({
    method: "GET",
    url: "/api/v1/accounts",
  });
}

export function deleteAccount(
  id: string,
  account: DeleteAccount,
): Promise<void> {
  return request({
    method: "DELETE",
    url: "/api/v1/accounts/" + id,
    body: account,
  });
}

export function changePassword(
  id: string,
  update: ChangePassword,
): Promise<void> {
  return request({
    method: "PUT",
    url: "/api/v1/accounts/" + id + "/password",
    body: update,
  });
}

export function signIn(signIn: SignIn): Promise<void> {
  return request({
    method: "POST",
    url: "/api/v1/accounts/sign-in",
    body: signIn,
  });
}

export function signOut(): Promise<void> {
  return request({
    method: "POST",
    url: "/api/v1/accounts/sign-out",
  });
}

export function signOutOther(): Promise<void> {
  return request({
    method: "POST",
    url: "/api/v1/accounts/sign-out-other",
  });
}
