create table app_sessions
(
    id          text   not null primary key,
    app_id      text   not null references apps (id) on delete cascade,
    secret_hash text   not null,
    created     bigint not null,
    expires     bigint not null
);
