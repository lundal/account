import { createResource, For, JSX, Show } from "solid-js";
import {
  Card,
  Column,
  H1,
  H2,
  Header,
  Page,
  Spinner,
  Button,
} from "@lundal/zed-solid";
import { RequestError } from "@lundal/request";
import { Account } from "../api/models.ts";
import { Navigation } from "../components/Navigation.tsx";
import { AppIcon } from "../components/AppIcon.tsx";
import { createInvite, listInvites } from "../api/invites.ts";
import { copyToClipboard } from "../utils/ClipboardUtils.ts";

type Props = {
  account: Account;
};

export function InvitesPage(props: Props): JSX.Element {
  const [invites, actions] = createResource(listInvites);

  function onCreateInvite(): void {
    createInvite()
      .then(() => actions.refetch())
      .catch((error: RequestError) => alert(error.message)); // TODO
  }

  return (
    <Page layout="3" theme="dark">
      <Header appIcon={<AppIcon />} appName="Account" />
      <Navigation account={props.account} />
      <main style={{ padding: "24px" }}>
        <Column>
          <H1
            style={{
              "padding-left": "18px",
              "border-left": "3px solid var(--z-border)",
            }}
          >
            Invites
          </H1>
          <Card
            style={{
              width: "100%",
              "min-width": "min-content",
              "max-width": "1000px",
            }}
          >
            <Column>
              <H2>Invites</H2>
              <p>
                Invites are necessary for creating accounts. They are valid for
                7 days.
              </p>
              <Button
                type="secondary"
                label="Create invite"
                onClick={onCreateInvite}
              />
              <Show when={invites()} fallback={<Spinner size="large" />}>
                {(invites) => (
                  <table class="z-table" style={{ "white-space": "nowrap" }}>
                    <thead>
                      <tr>
                        <th>Id</th>
                        <th>Used</th>
                        <th>Created</th>
                        <th>Expires</th>
                        <th>Actions</th>
                      </tr>
                    </thead>
                    <tbody>
                      <For
                        each={invites().toSorted(
                          (a, b) => b.created - a.created,
                        )}
                      >
                        {(invite) => (
                          <tr>
                            <td>{invite.id}</td>
                            <td>{invite.used ? "Yes" : "No"}</td>
                            <td>{new Date(invite.created).toLocaleString()}</td>
                            <td>{new Date(invite.expires).toLocaleString()}</td>
                            <td>
                              <Button
                                type="secondary"
                                label="Copy"
                                onClick={() => copyToClipboard(invite.id)}
                              />
                            </td>
                          </tr>
                        )}
                      </For>
                    </tbody>
                  </table>
                )}
              </Show>
            </Column>
          </Card>
        </Column>
      </main>
    </Page>
  );
}
