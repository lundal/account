export type Config = {
  account?: Account;
  appLinks: AppLink[];
};

export type AppLink = {
  name: string;
  iconUrl: string;
  linkUrl: string;
};

export type CreateAccount = {
  username: string;
  password: string;
  invite: string;
};

export type DeleteAccount = {
  password: string;
};

export type Account = {
  id: string;
  username: string;
  type: AccountType;
};

export type SignIn = {
  username: string;
  password: string;
};

export type ChangePassword = {
  currentPassword: string;
  newPassword: string;
};

export type AccountType = "NORMAL" | "ADMIN";

export type Invite = {
  id: string;
  used: boolean;
  created: number;
  expires: number;
};

export type Session = {
  id: string;
  created: number;
  expires: number;
  userAgent?: string;
  operatingSystem?: string;
  device?: string;
  current: boolean;
};
