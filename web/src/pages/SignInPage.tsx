import { JSX } from "solid-js";
import {
  Button,
  Card,
  Column,
  Field,
  Form,
  H1,
  Header,
  Link,
  Page,
  TextBox,
  createForm,
} from "@lundal/zed-solid";
import { RequestError } from "@lundal/request";
import { validate } from "../validation/validator.ts";
import { alphanum, maxChars, minChars, required } from "../validation/rules.ts";
import { signIn } from "../api/accounts.ts";
import { useLocation } from "@solidjs/router";
import { AppIcon } from "../components/AppIcon.tsx";
import { ErrorMessage } from "../components/ErrorMessage.tsx";

type Fields = {
  username: string;
  password: string;
};

type Errors = {
  username?: string;
  password?: string;
};

type Props = {
  refreshConfig: () => void;
};

const usernameRules = [required(), minChars(3), maxChars(60), alphanum()];
const passwordRules = [required(), minChars(12), maxChars(60)];

export function SignInPage(props: Props): JSX.Element {
  const location = useLocation();

  const form = createForm<Fields, Errors, void, RequestError>({
    initiator: () => ({
      username: "",
      password: "",
    }),
    validator: (fields) => ({
      username: validate("Username", fields.username, usernameRules),
      password: validate("Password", fields.password, passwordRules),
    }),
    submitter: (fields) =>
      signIn({
        username: fields.username,
        password: fields.password,
      }),
    onSuccess: () => props.refreshConfig(),
  });

  function errorMessage(): JSX.Element {
    const state = form.state();
    if (state.type == "Failure") {
      return <ErrorMessage error={state.error} />;
    }
  }

  return (
    <Page layout="3" theme="dark">
      <Header appIcon={<AppIcon />} appName="Account" />
      <main style={{ padding: "24px" }}>
        <Card style={{ margin: "0 auto", "max-width": "480px" }}>
          <Form onSubmit={form.submit}>
            <Column>
              <H1 lookLike="h2">Sign in</H1>
              <div style={{ width: "100%" }}>
                <Field
                  id="username"
                  label="Username"
                  error={form.errors().username}
                  children={(extra) => (
                    <TextBox
                      {...extra()}
                      autocomplete="username"
                      value={form.fields().username}
                      onChange={(value) => form.update({ username: value })}
                    />
                  )}
                />
                <Field
                  id="password"
                  label="Password"
                  error={form.errors().password}
                  children={(extra) => (
                    <TextBox
                      {...extra()}
                      autocomplete="current-password"
                      hidden={true}
                      value={form.fields().password}
                      onChange={(value) => form.update({ password: value })}
                    />
                  )}
                />
              </div>
              <Button
                type="primary"
                label="Sign in"
                onClick="submit"
                busy={form.busy()}
              />
              {errorMessage()}
              <p style={{ "margin-top": "12px" }}>
                Don't have an account?{" "}
                <Link href={"/create-account" + location.search}>
                  Create account!
                </Link>
              </p>
            </Column>
          </Form>
        </Card>
      </main>
    </Page>
  );
}
