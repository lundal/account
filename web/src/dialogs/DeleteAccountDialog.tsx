import { JSX } from "solid-js";
import {
  Button,
  Column,
  Dialog,
  Field,
  Form,
  H1,
  Message,
  Row,
  TextBox,
  createForm,
} from "@lundal/zed-solid";
import { RequestError } from "@lundal/request";
import { deleteAccount } from "../api/accounts.ts";
import { maxChars, minChars, required } from "../validation/rules.ts";
import { validate } from "../validation/validator.ts";
import { ErrorMessage } from "../components/ErrorMessage.tsx";

type Fields = {
  password: string;
};

type Errors = {
  password?: string;
};

type Props = {
  accountId: string;
  onDelete: () => void;
  onClose: () => void;
};

const passwordRules = [required(), minChars(12), maxChars(60)];

export function DeleteAccountDialog(props: Props): JSX.Element {
  const form = createForm<Fields, Errors, void, RequestError>({
    initiator: () => ({
      password: "",
    }),
    validator: (fields) => ({
      password: validate("Password", fields.password, passwordRules),
    }),
    submitter: (fields) =>
      deleteAccount(props.accountId, {
        password: fields.password,
      }),
    onSuccess: () => {
      props.onDelete();
      props.onClose();
    },
  });

  function errorMessage(): JSX.Element {
    const state = form.state();
    if (state.type == "Failure") {
      return <ErrorMessage error={state.error} />;
    }
  }

  return (
    <Dialog
      style={{ width: "100%", "max-width": "480px" }}
      onClose={props.onClose}
      children={(close) => (
        <Form onSubmit={form.submit}>
          <Column>
            <H1 lookLike="h2">Delete account</H1>
            <Message
              type="warning"
              text="There is no way to restore a deleted account"
            />
            <div style={{ width: "100%" }}>
              <Field
                id="password"
                label="Password"
                error={form.errors().password}
                children={(extra) => (
                  <TextBox
                    {...extra()}
                    autocomplete="current-password"
                    hidden={true}
                    value={form.fields().password}
                    onChange={(value) => form.update({ password: value })}
                  />
                )}
              />
            </div>
            <Row>
              <Button
                type="primary"
                label="Delete account"
                onClick="submit"
                busy={form.busy()}
              />
              <Button type="secondary" label="Cancel" onClick={close} />
            </Row>
            {errorMessage()}
          </Column>
        </Form>
      )}
    />
  );
}
