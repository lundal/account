import { expect, test } from "@playwright/test";
import { goto, signIn } from "./utils";

test.beforeEach(async ({ page }) => {
  await signIn(page, "normal");
});

test.describe("Account", () => {
  test.beforeEach(async ({ page }) => {
    await goto(page, "/account");
  });

  test("should display username", async ({ page }) => {
    await expect(page.locator("main")).toContainText("normal");
  });

  test("should display nav links", async ({ page }) => {
    await expect(page.locator("nav a")).toHaveText(["Account", "Security"]);
  });
});
