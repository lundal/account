import { createResource, For, JSX, Show } from "solid-js";
import { Card, Column, H1, H2, Header, Page, Spinner } from "@lundal/zed-solid";
import { Account } from "../api/models.ts";
import { Navigation } from "../components/Navigation.tsx";
import { AppIcon } from "../components/AppIcon.tsx";
import { listAccounts } from "../api/accounts.ts";

type Props = {
  accounts: Account;
};

export function AccountsPage(props: Props): JSX.Element {
  const [accounts, _] = createResource(listAccounts);

  return (
    <Page layout="3" theme="dark">
      <Header appIcon={<AppIcon />} appName="Account" />
      <Navigation account={props.accounts} />
      <main style={{ padding: "24px" }}>
        <Column>
          <H1
            style={{
              "padding-left": "18px",
              "border-left": "3px solid var(--z-border)",
            }}
          >
            Accounts
          </H1>
          <Card
            style={{
              width: "100%",
              "min-width": "min-content",
              "max-width": "600px",
            }}
          >
            <Column>
              <H2>Registered accounts</H2>
              <Show when={accounts()} fallback={<Spinner size="large" />}>
                {(accounts) => (
                  <table class="z-table" style={{ "white-space": "nowrap" }}>
                    <thead>
                      <tr>
                        <th>Id</th>
                        <th>Username</th>
                        <th>Type</th>
                      </tr>
                    </thead>
                    <tbody>
                      <For each={accounts()}>
                        {(account) => (
                          <tr>
                            <td>{account.id}</td>
                            <td>{account.username}</td>
                            <td>{account.type}</td>
                          </tr>
                        )}
                      </For>
                    </tbody>
                  </table>
                )}
              </Show>
            </Column>
          </Card>
        </Column>
      </main>
    </Page>
  );
}
