import { JSX } from "solid-js";
import {
  Button,
  Card,
  Column,
  Field,
  Form,
  H1,
  Header,
  Link,
  Page,
  TextBox,
  createForm,
} from "@lundal/zed-solid";
import { RequestError } from "@lundal/request";
import { validate } from "../validation/validator.ts";
import {
  alphanum,
  chars,
  maxChars,
  minChars,
  required,
} from "../validation/rules.ts";
import { createAccount } from "../api/accounts.ts";
import { useNavigate } from "@solidjs/router";
import { Account } from "../api/models.ts";
import { AppIcon } from "../components/AppIcon.tsx";
import { ErrorMessage } from "../components/ErrorMessage.tsx";

type Fields = {
  invite: string;
  username: string;
  password: string;
  passwordAgain: string;
};

type Errors = {
  invite?: string;
  username?: string;
  password?: string;
  passwordAgain?: string;
};

const inviteRules = [required(), chars(24)];
const usernameRules = [required(), minChars(3), maxChars(60), alphanum()];
const passwordRules = [required(), minChars(12), maxChars(60)];

export function CreateAccountPage(): JSX.Element {
  const navigate = useNavigate();

  const form = createForm<Fields, Errors, Account, RequestError>({
    initiator: () => ({
      invite: "",
      username: "",
      password: "",
      passwordAgain: "",
    }),
    validator: (fields) => ({
      invite: validate("Invite", fields.invite, inviteRules),
      username: validate("Username", fields.username, usernameRules),
      password: validate("Password", fields.password, passwordRules),
      passwordAgain: validate("", fields.passwordAgain, [
        {
          isValid: () => fields.password == fields.passwordAgain,
          message: () => "Passwords do not match",
        },
      ]),
    }),
    submitter: (fields) =>
      createAccount({
        invite: fields.invite,
        username: fields.username,
        password: fields.password,
      }),
    onSuccess: () => {
      const params = new URLSearchParams(window.location.search);
      params.set("created", "true");
      navigate("/sign-in?" + params.toString());
    },
  });

  function errorMessage(): JSX.Element {
    const state = form.state();
    if (state.type == "Failure") {
      return <ErrorMessage error={state.error} />;
    }
  }

  return (
    <Page layout="3" theme="dark">
      <Header appIcon={<AppIcon />} appName="Account" />
      <main style={{ padding: "24px" }}>
        <Card style={{ margin: "0 auto", "max-width": "480px" }}>
          <Form onSubmit={form.submit}>
            <Column>
              <H1 lookLike="h2">Create account</H1>
              <div style={{ width: "100%" }}>
                <Field
                  id="invite"
                  label="Invite"
                  error={form.errors().invite}
                  children={(extra) => (
                    <TextBox
                      {...extra()}
                      autocomplete="off"
                      value={form.fields().invite}
                      onChange={(value) => form.update({ invite: value })}
                    />
                  )}
                />
                <Field
                  id="username"
                  label="Username"
                  error={form.errors().username}
                  children={(extra) => (
                    <TextBox
                      {...extra()}
                      autocomplete="username"
                      value={form.fields().username}
                      onChange={(value) => form.update({ username: value })}
                    />
                  )}
                />
                <Field
                  id="password"
                  label="Password"
                  error={form.errors().password}
                  children={(extra) => (
                    <TextBox
                      {...extra()}
                      autocomplete="new-password"
                      hidden={true}
                      value={form.fields().password}
                      onChange={(value) => form.update({ password: value })}
                    />
                  )}
                />
                <Field
                  id="password-again"
                  label="Repeat password"
                  error={form.errors().passwordAgain}
                  children={(extra) => (
                    <TextBox
                      {...extra()}
                      autocomplete="new-password"
                      hidden={true}
                      value={form.fields().passwordAgain}
                      onChange={(value) =>
                        form.update({ passwordAgain: value })
                      }
                    />
                  )}
                />
                <p style={{ "padding-bottom": "24px" }}>
                  Please save your username and password in a password manager.
                  There is no way to recover your account if you loose them.
                </p>
                <p style={{ "padding-bottom": "24px" }}>
                  By creating an account you accept our{" "}
                  <a
                    class="z-link"
                    href="/terms-and-conditions"
                    target="_blank"
                  >
                    terms and conditions
                  </a>
                  .
                </p>
              </div>
              <Button
                type="primary"
                label="Create account"
                onClick="submit"
                busy={form.busy()}
              />
              {errorMessage()}
              <p style={{ "margin-top": "12px" }}>
                Already have an account?{" "}
                <Link href={"/sign-in" + window.location.search}>Sign in!</Link>
              </p>
            </Column>
          </Form>
        </Card>
      </main>
    </Page>
  );
}
