import { request } from "@lundal/request";
import { Invite } from "./models";

export function createInvite(): Promise<Invite> {
  return request({
    method: "POST",
    url: "/api/v1/invites",
  });
}

export function listInvites(): Promise<Invite[]> {
  return request({
    method: "GET",
    url: "/api/v1/invites",
  });
}
