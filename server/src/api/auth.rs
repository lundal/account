use chrono::Utc;

use crate::api::errors::ErrorResponse;
use crate::api::Context;
use crate::db;
use crate::utils::sha3_256_hex;

pub fn require_api_key(context: &Context) -> Result<(), ErrorResponse> {
    let api_key = context
        .authorization_header
        .as_ref()
        .ok_or(ErrorResponse::missing_authorization_header())?
        .strip_prefix("api-key ")
        .map(|s| s.to_string())
        .ok_or(ErrorResponse::unsupported_authentication_scheme())?;

    if !context.config.is_allowed_api_key(&api_key) {
        Err(ErrorResponse::invalid_api_key())?
    }

    Ok(())
}

pub async fn require_session(context: &Context) -> Result<db::Session, ErrorResponse> {
    let (id, secret) = context
        .session_cookie
        .as_ref()
        .ok_or(ErrorResponse::missing_session_cookie())?
        .split_once(":")
        .map(|(a, b)| (a.to_string(), b.to_string()))
        .ok_or(ErrorResponse::invalid_session_cookie())?;

    let session = db::get_session(&context.database, &id)
        .await
        .map_err(ErrorResponse::database)?
        .filter(|session| session.secret_hash == sha3_256_hex(&secret))
        .filter(|session| session.expires > Utc::now().timestamp_millis())
        .ok_or(ErrorResponse::invalid_session_cookie())?;

    Ok(session)
}

pub async fn require_admin_session(context: &Context) -> Result<db::Session, ErrorResponse> {
    let session = require_session(context).await?;

    db::get_account(&context.database, &session.account_id)
        .await
        .map_err(ErrorResponse::database)?
        .filter(|user| user.r#type == "ADMIN")
        .ok_or(ErrorResponse::permission_denied())?;

    Ok(session)
}
