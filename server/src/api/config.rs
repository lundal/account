use serde::{Deserialize, Serialize};

use crate::api::accounts::{to_api, Account};
use crate::api::auth::require_session;
use crate::api::errors::*;
use crate::api::Context;
use crate::db;

#[derive(Clone, Serialize, Deserialize)]
#[serde(rename_all = "camelCase")]
pub struct Config {
    pub account: Option<Account>,
    pub app_links: Vec<AppLink>,
}

#[derive(Clone, Serialize, Deserialize)]
#[serde(rename_all = "camelCase")]
pub struct AppLink {
    pub name: String,
    pub icon_url: String,
    pub link_url: String,
}

pub async fn get_config(context: Context) -> Result<Config, ErrorResponse> {
    return Ok(Config {
        account: match require_session(&context).await {
            Ok(session) => db::get_account(&context.database, &session.account_id)
                .await
                .map_err(ErrorResponse::database)?
                .map(to_api),
            Err(..) => None,
        },
        app_links: context
            .config
            .app_links
            .split(",")
            .map(|app_link| {
                let parts: Vec<&str> = app_link.split("|").collect();
                AppLink {
                    name: parts[0].to_string(),
                    icon_url: parts[1].to_string(),
                    link_url: parts[2].to_string(),
                }
            })
            .collect(),
    });
}
