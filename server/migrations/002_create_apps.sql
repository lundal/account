create table apps
(
    id          text   not null primary key,
    name        text   not null unique,
    secret_hash text   not null,
    created     bigint not null
);
