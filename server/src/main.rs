use std::error::Error;

use env_logger::Target;
use log::{info, LevelFilter};
use uaparser::UserAgentParser;

mod api;
mod config;
mod db;
mod jobs;
mod utils;

#[tokio::main]
async fn main() -> Result<(), Box<dyn Error>> {
    env_logger::builder()
        .target(Target::Stdout)
        .filter_level(LevelFilter::Warn)
        .filter_module(module_path!(), LevelFilter::Info)
        .try_init()?;

    info!("Parsing config");

    let config = config::parse();

    info!("Initializing UserAgentParser");

    let user_agent_parser = UserAgentParser::builder()
        // TODO: Drop device to reduce memory usage?
        .with_unicode_support(false)
        .build_from_bytes(include_bytes!("../uap-core/regexes.yaml"))
        .unwrap();

    info!("Connecting to database");

    let database = db::connect(&config.database_url).await?;

    info!("Starting background jobs");

    jobs::start(config.clone(), database.clone()).await;

    info!("Starting web server");

    api::run(config, database, user_agent_parser).await;

    Ok(())
}
