import { createMemo, JSX } from "solid-js";
import { RequestError } from "@lundal/request";
import { Message } from "@lundal/zed-solid";

type Props = {
  error: RequestError;
};

export function ErrorMessage(props: Props): JSX.Element {
  const text = createMemo(() => {
    switch (props.error.type) {
      case "Network":
        return "Network error";
      case "UnexpectedResponse":
        return "Unexpected response";
      case "Backend":
        return props.error.reason;
    }
  });
  return <Message type="error" text={text()} />;
}
