use sqlx::postgres::PgQueryResult;
use sqlx::{query, query_as, Error, FromRow, PgExecutor};

#[derive(Clone, Debug, FromRow)]
pub struct Invite {
    pub id: String,
    pub account_id: String,
    pub used: bool,
    pub created: i64,
    pub expires: i64,
}

pub async fn insert_invite<'e, E: PgExecutor<'e>>(
    e: E,
    invite: &Invite,
) -> Result<PgQueryResult, Error> {
    query(
        "insert into invites (id, account_id, used, created, expires) values ($1, $2, $3, $4, $5)",
    )
    .bind(&invite.id)
    .bind(&invite.account_id)
    .bind(&invite.used)
    .bind(&invite.created)
    .bind(&invite.expires)
    .execute(e)
    .await
}

pub async fn set_invite_used<'e, E: PgExecutor<'e>>(
    e: E,
    id: &String,
    used: bool,
) -> Result<PgQueryResult, Error> {
    query("update invites set used = $2 where id = $1")
        .bind(id)
        .bind(used)
        .execute(e)
        .await
}

pub async fn get_invite<'e, E: PgExecutor<'e>>(e: E, id: &String) -> Result<Option<Invite>, Error> {
    query_as("select * from invites where id = $1")
        .bind(id)
        .fetch_optional(e)
        .await
}

pub async fn list_invites<'e, E: PgExecutor<'e>>(e: E) -> Result<Vec<Invite>, Error> {
    query_as("select * from invites").fetch_all(e).await
}
