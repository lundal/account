use log::error;
use serde::{Deserialize, Serialize};
use warp::http::StatusCode;

#[derive(Clone, Serialize, Deserialize)]
pub struct ErrorResponse {
    pub code: u16,
    pub reason: String,
    pub details: Option<String>,
}

impl ErrorResponse {
    pub fn new(status: StatusCode, details: Option<String>) -> Self {
        ErrorResponse {
            code: status.as_u16(),
            reason: status.to_string(),
            details,
        }
    }

    pub fn account_not_found() -> Self {
        ErrorResponse {
            code: 404,
            reason: "Account not found".into(),
            details: None,
        }
    }

    pub fn session_not_found() -> Self {
        ErrorResponse {
            code: 404,
            reason: "Session not found".into(),
            details: None,
        }
    }

    pub fn username_taken() -> Self {
        ErrorResponse {
            code: 409,
            reason: "Username is taken".into(),
            details: None,
        }
    }

    pub fn account_creation_disabled() -> Self {
        ErrorResponse {
            code: 429,
            reason: "Account creation is currently disabled due to high demand".into(),
            details: None,
        }
    }

    pub fn invite_invalid() -> Self {
        ErrorResponse {
            code: 400,
            reason: "The invite is invalid, used or expired".into(),
            details: None,
        }
    }

    pub fn incorrect_username_or_password() -> Self {
        ErrorResponse {
            code: 400,
            reason: "Incorrect username or password".into(),
            details: None,
        }
    }

    pub fn incorrect_password() -> Self {
        ErrorResponse {
            code: 400,
            reason: "Incorrect password".into(),
            details: None,
        }
    }

    pub fn missing_authorization_header() -> Self {
        ErrorResponse {
            code: 401,
            reason: "Missing authorization header".into(),
            details: None,
        }
    }

    pub fn unsupported_authentication_scheme() -> Self {
        ErrorResponse {
            code: 401,
            reason: "Unsupported authentication scheme".into(),
            details: None,
        }
    }

    pub fn invalid_api_key() -> Self {
        ErrorResponse {
            code: 401,
            reason: "Invalid api key".into(),
            details: None,
        }
    }

    pub fn missing_session_cookie() -> Self {
        ErrorResponse {
            code: 401,
            reason: "Missing session cookie".into(),
            details: None,
        }
    }

    pub fn invalid_session_cookie() -> Self {
        ErrorResponse {
            code: 401,
            reason: "Invalid session cookie".into(),
            details: None,
        }
    }

    pub fn permission_denied() -> Self {
        ErrorResponse {
            code: 403,
            reason: "Permission denied".into(),
            details: None,
        }
    }

    pub fn database(error: sqlx::Error) -> Self {
        error!("Database error: {}", error);
        ErrorResponse {
            code: 500,
            reason: "Internal server error".into(),
            details: None,
        }
    }

    pub fn reqwest(error: reqwest::Error) -> Self {
        error!("Reqwest error: {}", error);
        ErrorResponse {
            code: 500,
            reason: "Internal server error".into(),
            details: None,
        }
    }

    pub fn bcrypt(error: bcrypt::BcryptError) -> Self {
        error!("Bcrypt error: {}", error);
        ErrorResponse {
            code: 500,
            reason: "Internal server error".into(),
            details: None,
        }
    }
}
