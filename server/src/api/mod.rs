use std::error::Error;
use std::path::Path;
use std::sync::Arc;

use include_dir::{include_dir, Dir};
use serde::Serialize;
use sqlx::{Pool, Postgres};
use uaparser::UserAgentParser;
use warp::http::StatusCode;
use warp::*;

use cookies::{with_cookies, Cookie};
use errors::*;

use crate::config::Config;

mod accounts;
mod auth;
mod common;
mod config;
mod cookies;
mod errors;
mod health;
mod invites;
mod sessions;

static STATIC: Dir<'_> = include_dir!("$CARGO_MANIFEST_DIR/static");

pub struct Context {
    authorization_header: Option<String>,
    user_agent_header: Option<String>,
    session_cookie: Option<String>,
    config: Config,
    database: Pool<Postgres>,
    user_agent_parser: Arc<UserAgentParser>,
}

pub async fn run(config: Config, database: Pool<Postgres>, user_agent_parser: UserAgentParser) {
    let port = config.port;
    serve(routes(config, database, user_agent_parser))
        .run(([0, 0, 0, 0], port))
        .await;
}

fn routes(
    config: Config,
    database: Pool<Postgres>,
    user_agent_parser: UserAgentParser,
) -> impl Filter<Extract = impl Reply, Error = Rejection> + Clone {
    let user_agent_parser = Arc::new(user_agent_parser);

    let with_context = || {
        // TODO: Figure out how to prevent double clone
        let config = config.clone();
        let database = database.clone();
        let user_agent_parser = user_agent_parser.clone();

        any()
            .and(header::optional("authorization"))
            .and(header::optional("user-agent"))
            .and(cookie::optional("session"))
            .map(
                move |authorization_header: Option<String>,
                      user_agent_header: Option<String>,
                      session_cookie: Option<String>| {
                    Context {
                        authorization_header,
                        user_agent_header,
                        session_cookie,
                        config: config.clone(),
                        database: database.clone(),
                        user_agent_parser: user_agent_parser.clone(),
                    }
                },
            )
    };

    let get_health = path!("health")
        .and(get())
        .then(health::get_health)
        .map(ok)
        .boxed();

    let get_config = path!("v1" / "config")
        .and(get())
        .and(with_context())
        .then(config::get_config)
        .map(ok)
        .boxed();

    let create_account = path!("v1" / "accounts")
        .and(post())
        .and(body::json())
        .and(with_context())
        .then(accounts::create_account)
        .map(created)
        .boxed();

    let list_accounts = path!("v1" / "accounts")
        .and(get())
        .and(with_context())
        .then(accounts::list_accounts)
        .map(ok)
        .boxed();

    let get_account = path!("v1" / "accounts" / String)
        .and(get())
        .and(with_context())
        .then(accounts::get_account)
        .map(ok)
        .boxed();

    let delete_account = path!("v1" / "accounts" / String)
        .and(delete())
        .and(body::json())
        .and(with_context())
        .then(accounts::delete_account)
        .map(no_content)
        .boxed();

    let change_password = path!("v1" / "accounts" / String / "password")
        .and(put())
        .and(body::json())
        .and(with_context())
        .then(accounts::change_password)
        .map(no_content)
        .boxed();

    let identify_account = path!("v1" / "accounts" / "identify")
        .and(post())
        .and(body::json())
        .and(with_context())
        .then(accounts::identify_account)
        .map(ok)
        .boxed();

    let sign_in = path!("v1" / "accounts" / "sign-in")
        .and(post())
        .and(body::json())
        .and(with_context())
        .then(accounts::sign_in)
        .map(set_cookies)
        .boxed();

    let sign_out = path!("v1" / "accounts" / "sign-out")
        .and(post())
        .and(with_context())
        .then(accounts::sign_out)
        .map(set_cookies)
        .boxed();

    let sign_out_other = path!("v1" / "accounts" / "sign-out-other")
        .and(post())
        .and(with_context())
        .then(accounts::sign_out_other)
        .map(no_content)
        .boxed();

    let create_invite = path!("v1" / "invites")
        .and(post())
        .and(with_context())
        .then(invites::create_invite)
        .map(ok)
        .boxed();

    let list_invites = path!("v1" / "invites")
        .and(get())
        .and(with_context())
        .then(invites::list_invites)
        .map(ok)
        .boxed();

    let list_sessions = path!("v1" / "sessions")
        .and(get())
        .and(with_context())
        .then(sessions::list_sessions)
        .map(ok)
        .boxed();

    let delete_session = path!("v1" / "sessions" / String)
        .and(delete())
        .and(with_context())
        .then(sessions::delete_session)
        .map(no_content)
        .boxed();

    let api_routes = get_health
        .or(get_config)
        .or(create_account)
        .or(list_accounts)
        .or(get_account)
        .or(delete_account)
        .or(change_password)
        .or(identify_account)
        .or(sign_in)
        .or(sign_out)
        .or(sign_out_other)
        .or(create_invite)
        .or(list_invites)
        .or(list_sessions)
        .or(delete_session)
        .recover(rejection_to_json);

    let statics = path::tail().and(get()).and_then(serve_static_files);

    path("api")
        .and(api_routes)
        .or(statics)
        .with(reply::with::header("cache-control", "no-store"))
        .with(reply::with::header(
            "content-security-policy",
            "default-src 'self'; img-src *; style-src 'self' 'unsafe-inline'", // TODO
        ))
}

fn set_cookies(result: Result<Vec<Cookie>, ErrorResponse>) -> Box<dyn Reply> {
    match result {
        Ok(cookies) => Box::new(with_cookies(
            reply::with_status(reply(), StatusCode::NO_CONTENT),
            cookies,
        )),
        Err(err) => Box::new(reply::with_status(
            reply::json(&err),
            StatusCode::from_u16(err.code).unwrap(),
        )),
    }
}

fn ok<T: Serialize>(result: Result<T, ErrorResponse>) -> impl Reply {
    match result {
        Ok(res) => reply::with_status(reply::json(&res), StatusCode::OK),
        Err(err) => reply::with_status(reply::json(&err), StatusCode::from_u16(err.code).unwrap()),
    }
}

fn created<T: Serialize>(result: Result<T, ErrorResponse>) -> impl Reply {
    match result {
        Ok(res) => reply::with_status(reply::json(&res), StatusCode::CREATED),
        Err(err) => reply::with_status(reply::json(&err), StatusCode::from_u16(err.code).unwrap()),
    }
}

fn no_content(result: Result<(), ErrorResponse>) -> impl Reply {
    match result {
        Ok(res) => reply::with_status(reply::json(&res), StatusCode::NO_CONTENT),
        Err(err) => reply::with_status(reply::json(&err), StatusCode::from_u16(err.code).unwrap()),
    }
}

async fn serve_static_files(tail: path::Tail) -> Result<impl Reply, Rejection> {
    let path = if !tail.as_str().starts_with("assets/") {
        Path::new("index.html")
    } else {
        Path::new(tail.as_str())
    };
    if let Some(file) = STATIC.get_file(path) {
        let content_type = mime_guess::from_path(path)
            .first_or_octet_stream()
            .to_string();
        Ok(reply::with_header(
            file.contents(),
            "content-type",
            content_type,
        ))
    } else {
        Err(reject::not_found())
    }
}

async fn rejection_to_json(rejection: Rejection) -> Result<impl Reply, Rejection> {
    if rejection.is_not_found() {
        let status = StatusCode::NOT_FOUND;
        let error = ErrorResponse::new(status, None);
        Ok(reply::with_status(reply::json(&error), status))
    } else if let Some(e) = rejection.find::<body::BodyDeserializeError>() {
        let status = StatusCode::BAD_REQUEST;
        let error = ErrorResponse::new(status, e.source().map(|cause| cause.to_string()));
        Ok(reply::with_status(reply::json(&error), status))
    } else {
        Err(rejection)
    }
}
