import { Rule } from "./validator.ts";

const integerPattern = /^-?[0-9]+$/;
const floatPattern = /^-?[0-9]+(\.[0-9]+)?$/;
const alphanumPattern = /^[A-Za-z0-9]+$/;

export function required(): Rule {
  return {
    isValid: (value) => !!value,
    message: (name) => `${name} is required`,
  };
}

export function minChars(n: number): Rule {
  return {
    isValid: (value) => value.length >= n,
    message: (name) => `${name} must be at least ${n} characters`,
  };
}

export function maxChars(n: number): Rule {
  return {
    isValid: (value) => value.length <= n,
    message: (name) => `${name} must be at most ${n} characters`,
  };
}

export function chars(n: number): Rule {
  return {
    isValid: (value) => value.length == n,
    message: (name) => `${name} must be exactly ${n} characters`,
  };
}

export function charsBetween(min: number, max: number): Rule {
  return {
    isValid: (value) => value.length >= min && value.length <= max,
    message: (name) => `${name} must be between ${min} and ${max} characters`,
  };
}

export function integer(min: number, max: number): Rule {
  return {
    isValid: (value) =>
      !!value.match(integerPattern) &&
      parseInt(value) >= 0 &&
      parseInt(value) <= max,
    message: (name) =>
      `${name} must be a whole number between ${min} and ${max}`,
  };
}

export function float(min: number, max: number): Rule {
  return {
    isValid: (value) =>
      !!value.match(floatPattern) &&
      parseFloat(value) >= min &&
      parseFloat(value) <= max,
    message: (name) => `${name} must be a number between ${min} and ${max}`,
  };
}

export function alphanum(): Rule {
  return {
    isValid: (value) => !!value.match(alphanumPattern),
    message: (name) => `${name} must contain only letters and digits`,
  };
}
