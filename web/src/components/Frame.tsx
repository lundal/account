import { JSX, Show } from "solid-js";
import { Header, Page } from "@lundal/zed-solid";
import { Account } from "../api/models.ts";
import { Navigation } from "./Navigation.tsx";
import { AppIcon } from "./AppIcon.tsx";

type Props = {
  account?: Account;
  children: JSX.Element;
};

export function Frame(props: Props): JSX.Element {
  return (
    <Page layout="3" theme="dark">
      <Header appIcon={<AppIcon />} appName="Account" />
      <Show when={props.account}>
        {(account) => <Navigation account={account()} />}
      </Show>
      <main>{props.children}</main>
    </Page>
  );
}
