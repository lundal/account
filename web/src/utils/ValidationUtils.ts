export type Validator<T> = (value: T) => string | undefined;
export type Rule<T> = [(value: T) => boolean, string];

export function validator<T>(rules: Rule<T>[]): Validator<T> {
  return (value: T) => {
    for (const [test, message] of rules) {
      if (!test(value)) {
        return message;
      }
    }
    return undefined;
  };
}

const integerPattern = /^-?[0-9]+$/;
const floatPattern = /^-?[0-9]+(\.[0-9]+)?$/;

export function required<T>(): (value: T) => boolean {
  return (value) => !!value;
}

export function length<T>(length: number): (value: ArrayLike<T>) => boolean {
  return (value) => value.length == length;
}

export function minLength<T>(length: number): (value: ArrayLike<T>) => boolean {
  return (value) => value.length >= length;
}

export function maxLength<T>(length: number): (value: ArrayLike<T>) => boolean {
  return (value) => value.length <= length;
}

export function integer(): (value: string) => boolean {
  return (value) => !!value.match(integerPattern);
}

export function float(): (value: string) => boolean {
  return (value) => !!value.match(floatPattern);
}

export function regex(pattern: RegExp): (value: string) => boolean {
  return (value) => !!value.match(pattern);
}

export function equals(other: string): (value: string) => boolean {
  return (value) => value === other;
}
