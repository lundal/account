import { JSX } from "solid-js";
import { Card, Column, H1, H2, Header, Page } from "@lundal/zed-solid";
import { AppIcon } from "../components/AppIcon.tsx";

export function TermsAndConditionsPage(): JSX.Element {
  return (
    <Page layout="3" theme="dark">
      <Header appIcon={<AppIcon />} appName="Account" />
      <main style={{ padding: "24px" }}>
        <Card style={{ margin: "0 auto", "max-width": "480px" }}>
          <Column>
            <H1>Terms and conditions</H1>
            <p>Last updated 2024-04-14</p>
            <H2>Your data</H2>
            <p>
              We will only ask for data that is strictly necessary to deliver
              our services. You agree that we may store and process any data you
              provide us. Your data will never be shared with third parties or
              used for profiling.
            </p>
            <H2>Termination</H2>
            <p>
              You can delete your account and all of its data at any time. Your
              account and all of its data will be deleted without warning after
              3 years of inactivity or if determined to engage in bad practices.
            </p>
            <H2>Liabilities</H2>
            <p>
              The services are provided "as is". We are not liable for any
              damages relating to the use of our services.
            </p>
            <H2>Changes</H2>
            <p>
              These terms and conditions may change without prior notice. You
              will get the option of accepting the new terms and conditions or
              deleting your account and all of its data.
            </p>
          </Column>
        </Card>
      </main>
    </Page>
  );
}
