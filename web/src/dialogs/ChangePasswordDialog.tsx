import { JSX } from "solid-js";
import { validate } from "../validation/validator";
import { changePassword } from "../api/accounts.ts";
import { maxChars, minChars, required } from "../validation/rules";
import {
  Button,
  Column,
  Field,
  Form,
  H1,
  TextBox,
  Dialog,
  Row,
  createForm,
} from "@lundal/zed-solid";
import { RequestError } from "@lundal/request";
import { ErrorMessage } from "../components/ErrorMessage.tsx";

type Fields = {
  currentPassword: string;
  newPassword: string;
  newPasswordAgain: string;
};

type Errors = {
  currentPassword?: string;
  newPassword?: string;
  newPasswordAgain?: string;
};

type Props = {
  accountId: string;
  onClose: () => void;
};

const passwordRules = [required(), minChars(12), maxChars(60)];

export function ChangePasswordDialog(props: Props): JSX.Element {
  const form = createForm<Fields, Errors, void, RequestError>({
    initiator: () => ({
      currentPassword: "",
      newPassword: "",
      newPasswordAgain: "",
    }),
    validator: (fields) => ({
      currentPassword: validate(
        "Current password",
        fields.currentPassword,
        passwordRules,
      ),
      newPassword: validate("New password", fields.newPassword, passwordRules),
      newPasswordAgain:
        fields.newPassword !== fields.newPasswordAgain
          ? "Passwords do not match"
          : undefined,
    }),
    submitter: (fields) =>
      changePassword(props.accountId, {
        currentPassword: fields.currentPassword,
        newPassword: fields.newPassword,
      }),
    onSuccess: () => props.onClose(),
  });

  function errorMessage(): JSX.Element {
    const state = form.state();
    if (state.type == "Failure") {
      return <ErrorMessage error={state.error} />;
    }
  }

  return (
    <Dialog
      style={{ width: "100%", "max-width": "480px" }}
      onClose={props.onClose}
      children={(close) => (
        <Form onSubmit={form.submit}>
          <Column>
            <H1 lookLike="h2">Change password</H1>
            <div style={{ width: "100%" }}>
              <Field
                id="current-password"
                label="Current password"
                error={form.errors().currentPassword}
                children={(extra) => (
                  <TextBox
                    {...extra()}
                    autocomplete="current-password"
                    hidden={true}
                    value={form.fields().currentPassword}
                    onChange={(value) =>
                      form.update({ currentPassword: value })
                    }
                  />
                )}
              />
              <Field
                id="new-password"
                label="New password"
                error={form.errors().newPassword}
                children={(extra) => (
                  <TextBox
                    {...extra()}
                    autocomplete="new-password"
                    hidden={true}
                    value={form.fields().newPassword}
                    onChange={(value) => form.update({ newPassword: value })}
                  />
                )}
              />
              <Field
                id="new-password-again"
                label="Repeat new password"
                error={form.errors().newPasswordAgain}
                children={(extra) => (
                  <TextBox
                    {...extra()}
                    autocomplete="new-password"
                    hidden={true}
                    value={form.fields().newPasswordAgain}
                    onChange={(value) =>
                      form.update({ newPasswordAgain: value })
                    }
                  />
                )}
              />
              <p style={{ "padding-bottom": "24px" }}>
                Please save your new password in a password manager. There is no
                way to recover your account if you loose it.
              </p>
            </div>
            <Row>
              <Button
                type="primary"
                label="Change password"
                onClick="submit"
                busy={form.busy()}
              />
              <Button type="secondary" label="Cancel" onClick={close} />
            </Row>
            {errorMessage()}
          </Column>
        </Form>
      )}
    />
  );
}
