FROM alpine:latest

RUN apk add --no-cache libgcc

# Install app
WORKDIR /app
COPY server/target/application ./application

EXPOSE 3200

CMD ["./application"]
