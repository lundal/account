use crate::utils::sha3_256_hex;
use std::env::var;

#[derive(Clone)]
pub struct Config {
    pub database_url: String,
    pub domain: String,
    pub port: u16,
    pub app_links: String,
    pub allowed_api_key_hashes: Vec<String>,
}

impl Config {
    pub fn is_test_mode(&self) -> bool {
        self.database_url.contains("test:test")
    }
    pub fn is_allowed_api_key(&self, api_key: &String) -> bool {
        let api_key_hash = sha3_256_hex(api_key);
        self.allowed_api_key_hashes.contains(&api_key_hash)
    }
}

pub fn parse() -> Config {
    Config {
        database_url: var("DATABASE_URL")
            .unwrap_or("postgres://test:test@localhost:3300/test".to_string()),
        domain: var("DOMAIN").unwrap_or("localhost".to_string()),
        port: var("PORT")
            .map(|value| value.parse().expect("invalid port"))
            .unwrap_or(3200),
        app_links: var("APP_LINKS")
            .unwrap_or("Account|http://localhost:3200/icon.svg|http://localhost:3200/".to_string()),
        allowed_api_key_hashes: var("ALLOWED_API_KEY_HASHES")
            .map(|value| value.split(",").map(|s| s.to_string()).collect())
            .unwrap_or(Vec::new()),
    }
}
