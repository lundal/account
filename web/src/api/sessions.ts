import { request } from "@lundal/request";
import { Session } from "./models";

export function listSessions(): Promise<Session[]> {
  return request({
    method: "GET",
    url: "/api/v1/sessions",
  });
}

export function deleteSession(id: string): Promise<void> {
  return request({
    method: "DELETE",
    url: "/api/v1/sessions/" + id,
  });
}
