use chrono::Duration;
use warp::http::{HeaderName, HeaderValue};
use warp::reply::Response;
use warp::Reply;

#[derive(Clone, Debug)]
pub struct Cookie {
    pub name: String,
    pub value: String,
    pub duration: Duration,
    pub domain: String,
    pub path: String,
    pub http_only: bool,
    pub secure: bool,
    pub same_site: String,
}

pub fn with_cookies<T: Reply>(reply: T, cookies: Vec<Cookie>) -> WithCookies<T> {
    WithCookies { cookies, reply }
}

#[derive(Debug)]
pub struct WithCookies<T> {
    cookies: Vec<Cookie>,
    reply: T,
}

impl<T: Reply> Reply for WithCookies<T> {
    fn into_response(self) -> Response {
        let mut res = self.reply.into_response();
        for cookie in self.cookies.iter() {
            let name = HeaderName::try_from("set-cookie");
            let value = HeaderValue::try_from(format!(
                "{}={}; Max-Age={}; Domain={}; Path={}; SameSite={}{}{}",
                cookie.name,
                cookie.value,
                cookie.duration.num_seconds(),
                cookie.domain,
                cookie.path,
                cookie.same_site,
                if cookie.http_only { "; HttpOnly" } else { "" },
                if cookie.secure { "; Secure" } else { "" },
            ));
            if let Ok(k) = name {
                if let Ok(v) = value {
                    res.headers_mut().append(k, v);
                }
            }
        }
        res
    }
}
