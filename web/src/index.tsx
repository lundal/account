import { createResource, JSX, Match, onMount, Show, Switch } from "solid-js";
import { render } from "solid-js/web";
import { Navigate, Route, Router, useNavigate } from "@solidjs/router";
import { getConfig } from "./api/config.ts";
import { LoadingPage } from "./pages/LoadingPage.tsx";
import { SignInPage } from "./pages/SignInPage.tsx";
import { AccountPage } from "./pages/AccountPage.tsx";
import { CreateAccountPage } from "./pages/CreateAccountPage.tsx";
import { TermsAndConditionsPage } from "./pages/TermsAndConditionsPage.tsx";
import { SecurityPage } from "./pages/SecurityPage.tsx";
import "./index.css";
import { AccountsPage } from "./pages/AccountsPage.tsx";
import { InvitesPage } from "./pages/InvitesPage.tsx";

function App(): JSX.Element {
  const [config, configActions] = createResource(getConfig);
  // TODO: Remove when all apps are updated to use standard urls
  if (window.location.hash.startsWith("#/")) {
    window.location.href =
      window.location.origin + window.location.hash.substring(1);
    return <LoadingPage />;
  }
  return (
    <Router>
      <Show when={config()} fallback={<LoadingPage />}>
        {(config) => (
          <Switch>
            <Match when={config().account}>
              {(account) => (
                <>
                  <Route
                    path="/account"
                    component={() => (
                      <AccountPage
                        account={account()}
                        refreshConfig={() => configActions.refetch()}
                      />
                    )}
                  />
                  <Route
                    path="/security"
                    component={() => (
                      <SecurityPage
                        account={account()}
                        refreshConfig={() => configActions.refetch()}
                      />
                    )}
                  />
                  <Route
                    path="/accounts"
                    component={() => <AccountsPage accounts={account()} />}
                  />
                  <Route
                    path="/invites"
                    component={() => <InvitesPage account={account()} />}
                  />
                  <Route
                    path="/sign-in"
                    component={() => <RedirectToPreviousPage />}
                  />
                  <Route
                    path="/terms-and-conditions"
                    component={() => <TermsAndConditionsPage />}
                  />
                  <Route
                    path="*"
                    component={() => <Navigate href="/account" />}
                  />
                </>
              )}
            </Match>
            <Match when={!config().account}>
              <Route
                path="/create-account"
                component={() => <CreateAccountPage />}
              />
              <Route
                path="/sign-in"
                component={() => (
                  <SignInPage refreshConfig={() => configActions.refetch()} />
                )}
              />
              <Route
                path="/terms-and-conditions"
                component={() => <TermsAndConditionsPage />}
              />
              <Route path="*" component={() => <RedirectToSignIn />} />
            </Match>
          </Switch>
        )}
      </Show>
    </Router>
  );
}
function RedirectToPreviousPage(): JSX.Element {
  const navigate = useNavigate();
  onMount(() => {
    const params = new URLSearchParams(window.location.search);
    const returnUrl = params.get("return") ?? "/";
    if (!isAllowedReturnUrl(returnUrl)) {
      // TODO: Show error message instead?
      navigate("/");
    } else if (returnUrl.startsWith("/")) {
      navigate(returnUrl);
    } else {
      window.location.href = returnUrl;
    }
  });
  return null;
}

function RedirectToSignIn(): JSX.Element {
  const navigate = useNavigate();
  onMount(() => {
    const params = new URLSearchParams();
    params.set("return", window.location.pathname);
    navigate("/sign-in?" + params.toString());
  });
  return null;
}

function isAllowedReturnUrl(returnUrl: string): boolean {
  const patterns = [
    /^https:\/\/[a-z-.]+\.lundal\.io\//,
    /^https:\/\/[a-z-.]+\.lundal\.dev\//,
    /^https:\/\/[a-z-.]+\.lundal\.test\//,
    /^http:\/\/localhost:\d+\//,
    /^\//,
  ];
  return patterns.some((pattern) => pattern.test(returnUrl));
}

render(() => <App />, document.getElementById("app")!);
