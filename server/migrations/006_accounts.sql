alter table users rename column name to username;
alter table users rename to accounts;

alter table user_sessions rename column user_id to account_id;
alter table user_sessions rename to sessions;

alter table user_invites rename column user_id to account_id;
alter table user_invites rename to invites;

drop table app_sessions;

drop table apps;
