use reqwest::Client;
use serde::{Deserialize, Serialize};

use crate::api::errors::*;

#[derive(Clone, Serialize, Deserialize)]
pub struct HealthResponse {
    pub status: String,
    pub ip: String,
}

pub async fn get_health() -> Result<HealthResponse, ErrorResponse> {
    let client = Client::new();
    let ip = client
        .get("https://icanhazip.com")
        .send()
        .await
        .map_err(ErrorResponse::reqwest)?
        .text()
        .await
        .map_err(ErrorResponse::reqwest)?;

    Ok(HealthResponse {
        status: "OK".into(),
        ip: ip.trim().into(),
    })
}
