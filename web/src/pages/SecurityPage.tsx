import { createResource, For, JSX, Show } from "solid-js";
import {
  Button,
  Card,
  Column,
  H1,
  H2,
  Header,
  Page,
  Row,
  Spinner,
} from "@lundal/zed-solid";
import { RequestError } from "@lundal/request";
import { Account } from "../api/models.ts";
import { Navigation } from "../components/Navigation.tsx";
import { AppIcon } from "../components/AppIcon.tsx";
import { deleteSession, listSessions } from "../api/sessions.ts";
import { signOut, signOutOther } from "../api/accounts.ts";

type Props = {
  account: Account;
  refreshConfig: () => void;
};

export function SecurityPage(props: Props): JSX.Element {
  const [sessions, actions] = createResource(listSessions);

  function onSignOut(): void {
    signOut()
      .then(() => props.refreshConfig())
      .catch((error: RequestError) => alert(error.message)); // TODO
  }

  function onSignOutOther(): void {
    signOutOther()
      .then(() => actions.refetch())
      .catch((error: RequestError) => alert(error.message)); // TODO
  }

  function onSignOutSession(id: string): void {
    deleteSession(id)
      .then(() => actions.refetch())
      .catch((error: RequestError) => alert(error.message)); // TODO
  }

  return (
    <Page layout="3" theme="dark">
      <Header appIcon={<AppIcon />} appName="Account" />
      <Navigation account={props.account} />
      <main style={{ padding: "24px" }}>
        <Column>
          <H1
            style={{
              "padding-left": "18px",
              "border-left": "3px solid var(--z-border)",
            }}
          >
            Security
          </H1>
          <Card
            style={{
              width: "100%",
              "min-width": "min-content",
              "max-width": "1000px",
            }}
          >
            <Column>
              <H2>Session management</H2>
              <p>
                Unless you sign out, your session will remain active for 30
                days.
              </p>
              <Row>
                <Button
                  type="secondary"
                  label="Sign out current session"
                  onClick={onSignOut}
                />
                <Button
                  type="secondary"
                  label="Sign out all other sessions"
                  onClick={onSignOutOther}
                />
              </Row>
              <Show when={sessions()} fallback={<Spinner size="large" />}>
                {(sessions) => (
                  <table class="z-table" style={{ "white-space": "nowrap" }}>
                    <thead>
                      <tr>
                        <th>Browser</th>
                        <th>System</th>
                        <th>Device</th>
                        <th>Signed in</th>
                        <th>Actions</th>
                      </tr>
                    </thead>
                    <tbody>
                      <For
                        each={sessions().toSorted(
                          (a, b) => b.created - a.created,
                        )}
                      >
                        {(session) => (
                          <tr>
                            <td>{session.userAgent ?? "Unknown"}</td>
                            <td>{session.operatingSystem ?? "Unknown"}</td>
                            <td>{session.device ?? "Unknown"}</td>
                            <td>
                              {new Date(session.created).toLocaleString()}
                            </td>
                            <td>
                              <Show
                                when={!session.current}
                                fallback={
                                  <div style={{ padding: "6px 0" }}>
                                    Current session
                                  </div>
                                }
                              >
                                <Button
                                  type="secondary"
                                  label="Sign out"
                                  onClick={() => onSignOutSession(session.id)}
                                />
                              </Show>
                            </td>
                          </tr>
                        )}
                      </For>
                    </tbody>
                  </table>
                )}
              </Show>
            </Column>
          </Card>
        </Column>
      </main>
    </Page>
  );
}
