use sqlx::postgres::PgQueryResult;
use sqlx::{query, query_as, Error, FromRow, PgExecutor};

#[derive(Clone, Debug, FromRow)]
pub struct Session {
    pub id: String,
    pub account_id: String,
    pub secret_hash: String,
    pub created: i64,
    pub expires: i64,
    pub user_agent: Option<String>,
    pub operating_system: Option<String>,
    pub device: Option<String>,
}

pub async fn insert_session<'e, E: PgExecutor<'e>>(
    e: E,
    session: &Session,
) -> Result<PgQueryResult, Error> {
    query("insert into sessions (id, account_id, secret_hash, created, expires, user_agent, operating_system, device) values ($1, $2, $3, $4, $5, $6, $7, $8)")
        .bind(&session.id)
        .bind(&session.account_id)
        .bind(&session.secret_hash)
        .bind(&session.created)
        .bind(&session.expires)
        .bind(&session.user_agent)
        .bind(&session.operating_system)
        .bind(&session.device)
        .execute(e)
        .await
}

pub async fn delete_session<'e, E: PgExecutor<'e>>(
    e: E,
    id: &String,
) -> Result<PgQueryResult, Error> {
    query("delete from sessions where id = $1")
        .bind(id)
        .execute(e)
        .await
}

pub async fn delete_sessions_expired_before<'e, E: PgExecutor<'e>>(
    e: E,
    time: i64,
) -> Result<PgQueryResult, Error> {
    query("delete from sessions where expires < $1")
        .bind(time)
        .execute(e)
        .await
}

pub async fn delete_sessions_except<'e, E: PgExecutor<'e>>(
    e: E,
    account_id: &String,
    session_id: &String,
) -> Result<PgQueryResult, Error> {
    query("delete from sessions where id != $2 and account_id = $1")
        .bind(account_id)
        .bind(session_id)
        .execute(e)
        .await
}

pub async fn get_session<'e, E: PgExecutor<'e>>(
    e: E,
    id: &String,
) -> Result<Option<Session>, Error> {
    query_as("select * from sessions where id = $1")
        .bind(id)
        .fetch_optional(e)
        .await
}

pub async fn list_sessions<'e, E: PgExecutor<'e>>(
    e: E,
    account_id: &String,
) -> Result<Vec<Session>, Error> {
    query_as("select * from sessions where account_id = $1")
        .bind(account_id)
        .fetch_all(e)
        .await
}
