import { createSignal, JSX } from "solid-js";
import { Button, Card, Column, H1, H2, Header, Page } from "@lundal/zed-solid";
import { Account } from "../api/models.ts";
import { Navigation } from "../components/Navigation.tsx";
import { ChangePasswordDialog } from "../dialogs/ChangePasswordDialog.tsx";
import { DeleteAccountDialog } from "../dialogs/DeleteAccountDialog.tsx";
import { AppIcon } from "../components/AppIcon.tsx";

type Props = {
  account: Account;
  refreshConfig: () => void;
};

export function AccountPage(props: Props): JSX.Element {
  const [dialog, setDialog] = createSignal<Dialog>({ type: "None" });

  function renderDialog(): JSX.Element | null {
    switch (dialog().type) {
      case "None":
        return null;
      case "ChangePassword":
        return (
          <ChangePasswordDialog
            accountId={props.account.id}
            onClose={() => setDialog({ type: "None" })}
          />
        );
      case "DeleteAccount":
        return (
          <DeleteAccountDialog
            accountId={props.account.id}
            onDelete={() => props.refreshConfig()}
            onClose={() => setDialog({ type: "None" })}
          />
        );
    }
  }

  return (
    <Page layout="3" theme="dark">
      <Header appIcon={<AppIcon />} appName="Account" />
      <Navigation account={props.account} />
      <main style={{ padding: "24px" }}>
        <Column>
          <H1
            style={{
              "padding-left": "18px",
              "border-left": "3px solid var(--z-border)",
            }}
          >
            Account
          </H1>
          <Card
            style={{
              width: "100%",
              "min-width": "min-content",
              "max-width": "600px",
            }}
          >
            <Column>
              <H2>Account information</H2>
              <div class="z-keyval">
                <div>Username</div>
                <div>{props.account.username}</div>
              </div>
              <div class="z-keyval">
                <div>Password</div>
                <Button
                  type="secondary"
                  label="Change password"
                  onClick={() => setDialog({ type: "ChangePassword" })}
                />
              </div>
            </Column>
          </Card>
          <Card
            style={{
              width: "100%",
              "min-width": "min-content",
              "max-width": "600px",
            }}
          >
            <Column>
              <H2>Delete account</H2>
              <p>
                This will permanently delete your account and all of its data.
              </p>
              <Button
                type="secondary"
                label="Delete account"
                onClick={() => setDialog({ type: "DeleteAccount" })}
              />
            </Column>
          </Card>
        </Column>
      </main>
      {renderDialog()}
    </Page>
  );
}

export type Dialog = None | ChangePassword | DeleteAccount;

type None = {
  type: "None";
};

type ChangePassword = {
  type: "ChangePassword";
};

type DeleteAccount = {
  type: "DeleteAccount";
};
