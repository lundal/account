create table users
(
    id               text   not null primary key,
    name             text   not null unique,
    password_hash    text   not null,
    type             text   not null,
    created          bigint not null
);

create table user_sessions
(
    id          text   not null primary key,
    user_id     text   not null references users (id) on delete cascade,
    secret_hash text   not null,
    created     bigint not null,
    expires     bigint not null
);

create table user_invites
(
    id      text    not null primary key,
    user_id text    not null references users (id) on delete cascade,
    used    boolean not null,
    created bigint  not null,
    expires bigint  not null
);
