use chrono::{Duration, Utc};
use log::error;
use sqlx::{Pool, Postgres};

use crate::config::Config;
use crate::db;

pub async fn start(config: Config, database: Pool<Postgres>) {
    tokio::spawn(async move { delete_expired_sessions(config.clone(), database.clone()).await });
}

pub async fn delete_expired_sessions(config: Config, database: Pool<Postgres>) {
    loop {
        tokio::time::sleep(Duration::minutes(10).to_std().unwrap()).await;

        let now = Utc::now().timestamp_millis();

        if let Err(error) = db::delete_sessions_expired_before(&database, now).await {
            error!("Database error: {}", error);
        }
    }
}
