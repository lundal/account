use chrono::{Duration, Utc};
use log::info;
use serde::{Deserialize, Serialize};
use uaparser::Parser;

use crate::api::auth::{require_admin_session, require_api_key, require_session};
use crate::api::common::*;
use crate::api::cookies::Cookie;
use crate::api::errors::*;
use crate::api::Context;
use crate::db;
use crate::utils::*;

#[derive(Clone, Serialize, Deserialize)]
pub struct CreateAccount {
    pub username: Username,
    pub password: Password,
    pub invite: Invite,
}

#[derive(Clone, Serialize, Deserialize)]
pub struct Account {
    pub id: String,
    pub username: String,
    pub r#type: String,
}

#[derive(Clone, Serialize, Deserialize)]
pub struct DeleteAccount {
    pub password: Password,
}

#[derive(Clone, Serialize, Deserialize)]
#[serde(rename_all = "camelCase")]
pub struct ChangePassword {
    pub current_password: Password,
    pub new_password: Password,
}

#[derive(Clone, Serialize, Deserialize)]
#[serde(rename_all = "camelCase")]
pub struct IdentifyAccount {
    pub session_cookie: String,
}

#[derive(Clone, Serialize, Deserialize)]
pub struct SignIn {
    pub username: Username,
    pub password: Password,
}

pub async fn create_account(
    request: CreateAccount,
    context: Context,
) -> Result<Account, ErrorResponse> {
    let mut tx = context
        .database
        .begin()
        .await
        .map_err(ErrorResponse::database)?;

    let hour_ago = Utc::now() - Duration::hours(1);
    let day_ago = Utc::now() - Duration::days(1);

    let created_last_hour = db::count_accounts_created_since(&mut *tx, hour_ago.timestamp_millis())
        .await
        .map_err(ErrorResponse::database)?;

    let created_last_day = db::count_accounts_created_since(&mut *tx, day_ago.timestamp_millis())
        .await
        .map_err(ErrorResponse::database)?;

    if created_last_hour >= 5 || created_last_day >= 10 {
        return Err(ErrorResponse::account_creation_disabled());
    }

    let invite = db::get_invite(&mut *tx, &request.invite.as_ref())
        .await
        .map_err(ErrorResponse::database)?
        .filter(|invite| invite.used == false)
        .filter(|invite| invite.expires > Utc::now().timestamp_millis())
        .ok_or(ErrorResponse::invite_invalid())?;

    if db::get_account_by_username(&mut *tx, request.username.as_ref())
        .await
        .map_err(ErrorResponse::database)?
        .is_some()
    {
        return Err(ErrorResponse::username_taken());
    }

    let id = random_id();

    let password_hash = bcrypt::hash(request.password.as_ref(), bcrypt::DEFAULT_COST)
        .map_err(ErrorResponse::bcrypt)?;

    let account = db::Account {
        id: id.clone(),
        username: request.username.clone().into(),
        password_hash: password_hash,
        r#type: "NORMAL".into(),
        created: Utc::now().timestamp_millis(),
    };

    db::set_invite_used(&mut *tx, &invite.id, true)
        .await
        .map_err(ErrorResponse::database)?;

    db::insert_account(&mut *tx, &account)
        .await
        .map_err(ErrorResponse::database)?;

    tx.commit().await.map_err(ErrorResponse::database)?;

    info!("Created account with id '{}'", account.id);

    return Ok(Account {
        id: id,
        username: account.username,
        r#type: account.r#type,
    });
}

pub async fn list_accounts(context: Context) -> Result<Vec<Account>, ErrorResponse> {
    require_api_key(&context).or(require_admin_session(&context).await.map(|_| ()))?;

    let accounts = db::list_accounts(&context.database)
        .await
        .map_err(ErrorResponse::database)?;

    return Ok(accounts.iter().cloned().map(to_api).collect());
}

pub async fn get_account(id: String, context: Context) -> Result<Account, ErrorResponse> {
    require_api_key(&context).or(require_session(&context).await.map(|_| ()))?;

    let account = db::get_account(&context.database, &id)
        .await
        .map_err(ErrorResponse::database)?
        .ok_or(ErrorResponse::account_not_found())?;

    return Ok(to_api(account));
}

pub async fn delete_account(
    id: String,
    request: DeleteAccount,
    context: Context,
) -> Result<(), ErrorResponse> {
    let session = require_session(&context).await?;

    if session.account_id != id {
        return Err(ErrorResponse::permission_denied());
    }

    let account = db::get_account(&context.database, &id)
        .await
        .map_err(ErrorResponse::database)?
        .ok_or(ErrorResponse::account_not_found())?;

    let password_verified =
        bcrypt::verify(request.password.as_ref(), account.password_hash.as_ref())
            .map_err(ErrorResponse::bcrypt)?;

    if !password_verified {
        return Err(ErrorResponse::incorrect_password());
    }

    db::delete_account(&context.database, &id)
        .await
        .map_err(ErrorResponse::database)?;

    info!("Deleted account with id '{}'", id);

    return Ok(());
}

pub async fn change_password(
    id: String,
    request: ChangePassword,
    context: Context,
) -> Result<(), ErrorResponse> {
    let session = require_session(&context).await?;

    if session.account_id != id {
        return Err(ErrorResponse::permission_denied());
    }

    let account = db::get_account(&context.database, &id)
        .await
        .map_err(ErrorResponse::database)?
        .ok_or(ErrorResponse::account_not_found())?;

    let password_verified = bcrypt::verify(
        request.current_password.as_ref(),
        account.password_hash.as_ref(),
    )
    .map_err(ErrorResponse::bcrypt)?;

    if !password_verified {
        return Err(ErrorResponse::incorrect_password());
    }

    let password_hash = bcrypt::hash(request.new_password.as_ref(), bcrypt::DEFAULT_COST)
        .map_err(ErrorResponse::bcrypt)?;

    db::set_account_password_hash(&context.database, &account.id, &password_hash)
        .await
        .map_err(ErrorResponse::database)?;

    info!("Changed password for account with id '{}'", id);

    return Ok(());
}

pub async fn identify_account(
    request: IdentifyAccount,
    context: Context,
) -> Result<Account, ErrorResponse> {
    require_api_key(&context)?;

    let (id, secret) = request
        .session_cookie
        .split_once(":")
        .map(|(a, b)| (a.to_string(), b.to_string()))
        .ok_or(ErrorResponse::invalid_session_cookie())?;

    let session = db::get_session(&context.database, &id)
        .await
        .map_err(ErrorResponse::database)?
        .filter(|session| session.secret_hash == sha3_256_hex(&secret))
        .filter(|session| session.expires > Utc::now().timestamp_millis())
        .ok_or(ErrorResponse::invalid_session_cookie())?;

    let account = db::get_account(&context.database, &session.account_id)
        .await
        .map_err(ErrorResponse::database)?
        .ok_or(ErrorResponse::account_not_found())?;

    return Ok(to_api(account));
}

pub async fn sign_in(request: SignIn, context: Context) -> Result<Vec<Cookie>, ErrorResponse> {
    if context.config.is_test_mode() {
        let username_exists =
            db::get_account_by_username(&context.database, request.username.as_ref())
                .await
                .map_err(ErrorResponse::database)?
                .is_some();

        if !username_exists {
            let r#type = if request.username.as_ref().starts_with("normal") {
                "NORMAL".into()
            } else if request.username.as_ref().starts_with("admin") {
                "ADMIN".into()
            } else {
                return Err(ErrorResponse::incorrect_username_or_password());
            };

            let password_hash = bcrypt::hash(request.password.as_ref(), bcrypt::DEFAULT_COST)
                .map_err(ErrorResponse::bcrypt)?;

            let account = db::Account {
                id: random_id(),
                username: request.username.clone().into(),
                password_hash: password_hash,
                r#type: r#type,
                created: Utc::now().timestamp_millis(),
            };

            db::insert_account(&context.database, &account)
                .await
                .map_err(ErrorResponse::database)?;

            info!("Created account with id '{}' (TEST MODE)", account.id);
        }
    }

    let account = db::get_account_by_username(&context.database, request.username.as_ref())
        .await
        .map_err(ErrorResponse::database)?
        .ok_or(ErrorResponse::incorrect_username_or_password())?; // TODO: dummy verification

    let password_verified =
        bcrypt::verify(request.password.as_ref(), account.password_hash.as_ref())
            .map_err(ErrorResponse::bcrypt)?;

    if !password_verified {
        return Err(ErrorResponse::incorrect_username_or_password());
    }

    let id = random_id();
    let secret = random_id();

    let session = db::Session {
        id: id.clone(),
        account_id: account.id,
        secret_hash: sha3_256_hex(&secret),
        created: Utc::now().timestamp_millis(),
        expires: (Utc::now() + Duration::days(30)).timestamp_millis(),
        user_agent: context
            .user_agent_header
            .as_ref()
            .map(|ua| context.user_agent_parser.parse_user_agent(ua))
            .map(|ua| ua.family.to_string())
            .filter(|s| s != "Other"),
        operating_system: context
            .user_agent_header
            .as_ref()
            .map(|ua| context.user_agent_parser.parse_os(ua))
            .map(|os| os.family.to_string())
            .filter(|s| s != "Other"),
        device: context
            .user_agent_header
            .as_ref()
            .map(|ua| context.user_agent_parser.parse_device(ua))
            .map(|d| d.family.to_string())
            .filter(|s| s != "Other"),
    };

    db::insert_session(&context.database, &session)
        .await
        .map_err(ErrorResponse::database)?;

    return Ok(vec![Cookie {
        name: "session".into(),
        value: format!("{}:{}", id, secret),
        domain: context.config.domain.clone(),
        path: "/api/".into(),
        duration: Duration::days(30),
        http_only: true,
        secure: !context.config.is_test_mode(),
        same_site: "Strict".into(),
    }]);
}

pub async fn sign_out(context: Context) -> Result<Vec<Cookie>, ErrorResponse> {
    let session = require_session(&context).await?;

    db::delete_session(&context.database, &session.id)
        .await
        .map_err(ErrorResponse::database)?;

    return Ok(vec![Cookie {
        name: "session".into(),
        value: "".into(),
        duration: Duration::zero(),
        domain: context.config.domain.clone(),
        path: "/api/".into(),
        http_only: true,
        secure: !context.config.is_test_mode(),
        same_site: "Strict".into(),
    }]);
}

pub async fn sign_out_other(context: Context) -> Result<(), ErrorResponse> {
    let session = require_session(&context).await?;

    db::delete_sessions_except(&context.database, &session.account_id, &session.id)
        .await
        .map_err(ErrorResponse::database)?;

    return Ok(());
}

pub(crate) fn to_api(account: db::Account) -> Account {
    Account {
        id: account.id,
        username: account.username,
        r#type: account.r#type,
    }
}
