import { JSX, Show } from "solid-js";
import { Nav, Icon, NavHeader, NavLink } from "@lundal/zed-solid";
import {
  faEnvelope,
  faHouseUser,
  faShield,
  faUsers,
} from "@fortawesome/free-solid-svg-icons";
import { Account } from "../api/models.ts";

type Props = {
  account: Account;
};

export function Navigation(props: Props): JSX.Element {
  return (
    <Nav>
      <NavHeader>Main</NavHeader>
      <NavLink
        href="/account"
        icon={<Icon definition={faHouseUser} />}
        text="Account"
      />
      <NavLink
        href="/security"
        icon={<Icon definition={faShield} />}
        text="Security"
      />
      <Show when={props.account.type === "ADMIN"}>
        <NavHeader>Admin</NavHeader>
        <NavLink
          href="/accounts"
          icon={<Icon definition={faUsers} />}
          text="Accounts"
        />
        <NavLink
          href="/invites"
          icon={<Icon definition={faEnvelope} />}
          text="Invites"
        />
      </Show>
    </Nav>
  );
}
