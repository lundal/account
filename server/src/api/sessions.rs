use serde::{Deserialize, Serialize};

use crate::api::auth::require_session;
use crate::api::errors::*;
use crate::api::Context;
use crate::db;

#[derive(Clone, Serialize, Deserialize)]
#[serde(rename_all = "camelCase")]
pub struct Session {
    pub id: String,
    pub created: i64,
    pub expires: i64,
    pub user_agent: Option<String>,
    pub operating_system: Option<String>,
    pub device: Option<String>,
    pub current: bool,
}

pub async fn list_sessions(context: Context) -> Result<Vec<Session>, ErrorResponse> {
    let active_session = require_session(&context).await?;

    let sessions = db::list_sessions(&context.database, &active_session.account_id)
        .await
        .map_err(ErrorResponse::database)?;

    return Ok(sessions
        .iter()
        .cloned()
        .map(|session| to_api(session, &active_session.id))
        .collect());
}

pub async fn delete_session(id: String, context: Context) -> Result<(), ErrorResponse> {
    let active_session = require_session(&context).await?;

    let session = db::get_session(&context.database, &id)
        .await
        .map_err(ErrorResponse::database)?
        .ok_or(ErrorResponse::session_not_found())?;

    if session.account_id != active_session.account_id {
        return Err(ErrorResponse::permission_denied());
    }

    db::delete_session(&context.database, &session.id)
        .await
        .map_err(ErrorResponse::database)?;

    Ok(())
}

pub(crate) fn to_api(session: db::Session, active_session_id: &String) -> Session {
    let current = &session.id == active_session_id;
    Session {
        id: session.id,
        created: session.created,
        expires: session.expires,
        user_agent: session.user_agent,
        operating_system: session.operating_system,
        device: session.device,
        current: current,
    }
}
