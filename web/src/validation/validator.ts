export type Rule = {
  isValid: (value: string) => boolean;
  message: (name: string) => string;
};

export function validate(
  name: string,
  value: string,
  rules: Rule[],
): string | undefined {
  for (const rule of rules) {
    if (!rule.isValid(value)) {
      return rule.message(name);
    }
  }
}
