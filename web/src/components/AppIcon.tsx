import { JSX } from "solid-js";

export function AppIcon(): JSX.Element {
  return (
    <svg
      xmlns="http://www.w3.org/2000/svg"
      width="360"
      height="360"
      viewBox="0 0 360 360"
      style="--bg: currentColor; --fg: var(--z-foreground);"
    >
      <rect width="360" height="360" rx="60" fill="var(--bg)" />
      <path
        d="M90 300C67.9086 300 49.4143 281.681 56.1056 260.627C62.3485 240.985 73.232 222.92 88.0761 208.076C112.456 183.696 145.522 170 180 170C214.478 170 247.544 183.696 271.924 208.076C286.768 222.92 297.651 240.984 303.894 260.627C310.586 281.681 292.091 300 270 300L180 300L90 300Z"
        fill="var(--fg)"
      />
      <circle
        cx="180"
        cy="120"
        r="80"
        fill="var(--fg)"
        stroke="var(--bg)"
        stroke-width="20"
      />
    </svg>
  );
}
