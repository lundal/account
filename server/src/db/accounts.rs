use sqlx::postgres::PgQueryResult;
use sqlx::{query, query_as, Error, FromRow, PgExecutor};

#[derive(Clone, Debug, FromRow)]
pub struct Account {
    pub id: String,
    pub username: String,
    pub password_hash: String,
    pub r#type: String,
    pub created: i64,
}

pub async fn insert_account<'e, E: PgExecutor<'e>>(
    e: E,
    account: &Account,
) -> Result<PgQueryResult, Error> {
    query("insert into accounts (id, username, password_hash, type, created) values ($1, $2, $3, $4, $5)")
        .bind(&account.id)
        .bind(&account.username)
        .bind(&account.password_hash)
        .bind(&account.r#type)
        .bind(&account.created)
        .execute(e)
        .await
}

pub async fn set_account_password_hash<'e, E: PgExecutor<'e>>(
    e: E,
    id: &String,
    password_hash: &String,
) -> Result<PgQueryResult, Error> {
    query("update accounts set password_hash = $2 where id = $1")
        .bind(&id)
        .bind(&password_hash)
        .execute(e)
        .await
}

pub async fn delete_account<'e, E: PgExecutor<'e>>(
    e: E,
    id: &String,
) -> Result<PgQueryResult, Error> {
    query("delete from accounts where id = $1")
        .bind(id)
        .execute(e)
        .await
}

pub async fn get_account<'e, E: PgExecutor<'e>>(
    e: E,
    id: &String,
) -> Result<Option<Account>, Error> {
    query_as("select * from accounts where id = $1")
        .bind(id)
        .fetch_optional(e)
        .await
}

pub async fn get_account_by_username<'e, E: PgExecutor<'e>>(
    e: E,
    id: &String,
) -> Result<Option<Account>, Error> {
    query_as("select * from accounts where username = $1")
        .bind(id)
        .fetch_optional(e)
        .await
}

pub async fn list_accounts<'e, E: PgExecutor<'e>>(e: E) -> Result<Vec<Account>, Error> {
    query_as("select * from accounts").fetch_all(e).await
}

pub async fn count_accounts_created_since<'e, E: PgExecutor<'e>>(
    e: E,
    time: i64,
) -> Result<i64, Error> {
    query_as("select count(*) from accounts where created > $1")
        .bind(time)
        .fetch_one(e)
        .await
        .map(|(count,)| count)
}
