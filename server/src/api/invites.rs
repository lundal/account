use chrono::{Duration, Utc};
use log::info;
use serde::{Deserialize, Serialize};

use crate::api::auth::require_admin_session;
use crate::api::errors::*;
use crate::api::Context;
use crate::db;
use crate::utils::*;

#[derive(Clone, Serialize, Deserialize)]
pub struct Invite {
    pub id: String,
    pub used: bool,
    pub created: i64,
    pub expires: i64,
}

pub async fn create_invite(context: Context) -> Result<Invite, ErrorResponse> {
    let session = require_admin_session(&context).await?;

    let id = random_id();

    let invite = db::Invite {
        id: id.clone(),
        account_id: session.account_id,
        used: false,
        created: Utc::now().timestamp_millis(),
        expires: (Utc::now() + Duration::days(7)).timestamp_millis(),
    };

    db::insert_invite(&context.database, &invite)
        .await
        .map_err(ErrorResponse::database)?;

    info!("Created an invite");

    return Ok(to_api(invite));
}

pub async fn list_invites(context: Context) -> Result<Vec<Invite>, ErrorResponse> {
    require_admin_session(&context).await?;

    let invites = db::list_invites(&context.database)
        .await
        .map_err(ErrorResponse::database)?;

    return Ok(invites.iter().cloned().map(to_api).collect());
}

fn to_api(invite: db::Invite) -> Invite {
    Invite {
        id: invite.id,
        used: invite.used,
        created: invite.created,
        expires: invite.expires,
    }
}
